package BikeMain;

import Bike.BikeFactory;
import Bike.IBike;

public class Main {
    public static void main(String[] args) {
        IBike felt = BikeFactory.create1seatFelt();
        felt.run();

    }
}
