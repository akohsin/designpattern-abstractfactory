package PCMain;


import PC.AbstractPC;
import PC.AbstractPCFactory;
import PC.ApplePC;

public class Main {
    public static void main(String[] args) {
        AbstractPC mac = ApplePC.createMacBook();
        AbstractPC macAir = AbstractPCFactory.createMacAir();

    }

}
