package PC;
//Zadanie 1:
//        Stwórz klasę abstrakcyjną AbstractPC która posiada:
//        pole tekst - nazwę komputera
//        pole COMPUTER_BRAND - markę komputera
//        pole cpu_power - moc komputera (int 0-100)
//        pole gpu_power - moc grafiki komputera (double 0.00 - 1.00)
//        pole isOverclocked - flaga definiująca czy komputer był "podrasowany"
//        COMPUTER_BRAND: ASUS, HP, SAMSUNG, APPLE
//        Stwórz klasy dziedziczące:
//        AsusPC, HpPC, SamsungPC, AppleMac które dziedziczą po komputerze.
//        W każdej klasie stwórz metody factory. Dla każdej marki wymyśl dwa komputery.

public abstract class AbstractPC {
    private String nazwaKomputera;
    private COMPUTER_BRAND marka;
    private int mocKomputera;
    private double  mocGrafiki;
    boolean czyPodkręcony;

    protected AbstractPC(String nazwaKomputera, COMPUTER_BRAND marka, int mocKomputera, double mocGrafiki, boolean czyPodkręcony) {
        this.nazwaKomputera = nazwaKomputera;
        this.marka = marka;
        this.mocKomputera = mocKomputera;
        this.mocGrafiki = mocGrafiki;
        this.czyPodkręcony = czyPodkręcony;
    }

    protected enum COMPUTER_BRAND{
        ASUS, HP, SAMSUNG, APPLE
    }

}
