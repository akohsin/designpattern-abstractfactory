package PC;

public class ApplePC extends AbstractPC {

    protected ApplePC(String nazwaKomputera, AbstractPC.COMPUTER_BRAND marka, int mocKomputera, double mocGrafiki,
                   boolean czyPodkręcony) {

        super(nazwaKomputera, marka, mocKomputera, mocGrafiki, czyPodkręcony);
    }

    public static ApplePC createMacBook()    {
        return new ApplePC("Macbook Air",COMPUTER_BRAND.APPLE,50,1.2, false);
    }
}
