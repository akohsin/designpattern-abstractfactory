package Bike;

//Należy stworzyć aplikację symulującą fabrykę rowerów. Klasa Bike: ma markę, ilość miejsc,
//        ilość przerzutek, oraz typ rowera (bicycle, tandem).
//        BIKE_TYPE: BICYCLE, TANDEM
//        Stwórz i przetestuj fabrykę abstrakcyjną (BikeFactory) która pozwala na tworzenie:
//        Rowerów jednoosbowych, marki KROSS, które mają 5 przerzutek
//        Rowerów jednoosbowych, marki MERIDA, które mają 6 przerzutek
//        Tandemów, marki INIANA, które mają 3 przerzutki
//        Rowerów jednoosbowych, marki FELT, które mają 6 przerzutek
//        Tandemów, marki GOETZE, które mają jedną przerzutkę
//        https://bitbucket.org/nordeagda2/designpatternfactorybikes

public abstract class BikeFactory {

    public static IBike create1seatKross() {
        return Bike.create(BIKE_BRAND.KROSS, 1, 5, BIKE_TYPE.BICYCLE);
    }

    public static IBike create1seatMerida() {
        return Bike.create(BIKE_BRAND.MERIDA, 1, 6, BIKE_TYPE.BICYCLE);
    }
    public static IBike create2seatIniana(){
        return Bike.create(BIKE_BRAND.INIANA, 2, 2, BIKE_TYPE.TANDEM);
    }
    public static IBike create1seatFelt(){
        return Bike.create(BIKE_BRAND.FELT, 1, 6, BIKE_TYPE.BICYCLE);
    }
    public static IBike create1setGoetze(){
        return Bike.create(BIKE_BRAND.GOETZE, 2, 1, BIKE_TYPE.TANDEM);
    }

}
