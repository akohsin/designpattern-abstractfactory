package Bike;

class Bike implements IBike {
    BIKE_BRAND brand;
    BIKE_TYPE type;
    int gears;
    int seats;

    public void run() {
        System.out.println("jade");
    }

    private Bike(BIKE_BRAND brand, int seats, int gears, BIKE_TYPE type) {
        this.brand = brand;
        this.type = type;
        this.gears = gears;
        this.seats = seats;
    }

    public static IBike create(BIKE_BRAND brand, int seats, int gears, BIKE_TYPE type) {
        return new Bike(brand, seats, gears, type);
    }
}
