package Car;

public class Car {
    private String model;
    private int speed;
    private int horsepower;
    private String manufacturer;
    private boolean hasLPG;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public boolean isHasLPG() {
        return hasLPG;
    }

    public void setHasLPG(boolean hasLPG) {
        this.hasLPG = hasLPG;
    }

    public Car(String model, int speed, int horsepower, String manufacturer, boolean hasLPG) {
        this.model = model;
        this.speed = speed;
        this.horsepower = horsepower;
        this.manufacturer = manufacturer;
        this.hasLPG = hasLPG;
    }
}
