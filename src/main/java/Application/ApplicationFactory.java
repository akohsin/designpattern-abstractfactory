package Application;

import java.time.LocalDateTime;
import java.util.List;

public class ApplicationFactory {

    public static Person createPerson(String name, String surname, long indeks) {
        return new Person(name, surname, indeks);
    }

    public static Application createConditionalStay(String miejsceUrodzenia, Person daneAplikanta,
                                                    String tresc, List<Double> grades, String reason) {
        return new ConditionalStayApplication(LocalDateTime.now() miejsceUrodzenia, daneAplikanta, tresc, grades, reason);
    }

    public static Application createSchoolarshipApplication(List<Double> grades, List<String> extraActivities) {
        return new SchoolarshipApplication(grades, extraActivities);
    }
}
