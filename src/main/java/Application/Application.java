package Application;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Application {
private LocalDateTime dataUtworzenia;
private String miejsceUrodzenia;
private Person daneAplikanta;
private String tresc;

    public Application(LocalDateTime dataUtworzenia, String miejsceUrodzenia, Person daneAplikanta, String tresc) {
        this.dataUtworzenia = dataUtworzenia;
        this.miejsceUrodzenia = miejsceUrodzenia;
        this.daneAplikanta = daneAplikanta;
        this.tresc = tresc;
    }
}
