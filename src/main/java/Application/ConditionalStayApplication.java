package Application;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ConditionalStayApplication extends Application {
    private List<Double> grades = new ArrayList<>();
    private String reason;

    public ConditionalStayApplication(LocalDateTime dataUtworzenia, String miejsceUrodzenia, Person daneAplikanta, String tresc,
                                      List<Double> grades, String reason) {
        super(dataUtworzenia, miejsceUrodzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.reason = reason;
    }
}
