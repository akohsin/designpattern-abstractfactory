package Application;

public class Person {
    private String name;
    private String surname;
    private long indeks;

    protected Person(String name, String surname, long indeks) {
        this.name = name;
        this.surname = surname;
        this.indeks = indeks;
    }
}
