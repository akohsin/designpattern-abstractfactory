package Application;

import java.util.ArrayList;
import java.util.List;


class SchoolarshipApplication extends Application {
    private List<Double> grades = new ArrayList<>();
    private List<String> extracurricularActivities = new ArrayList<>();

    public SchoolarshipApplication(List<Double> grades, List<String> extracurricularActivities) {
        this.grades = grades;
        this.extracurricularActivities = extracurricularActivities;
    }
}
